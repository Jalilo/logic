//
//  main.swift
//  transversedLines
//
//  Created by Omar on 24/11/20.
//

import Foundation

func transversedLines(cases: Int) -> Void {
    var grid = [[Int]]()
    
    for _ in 0...cases - 1 {
        guard let string = readLine() else { return }
        guard let rows    = Int(string.prefix(1)) else { return }
        guard let columns = Int(string.suffix(1)) else { return }
        
        let array = [rows, columns]
        grid.append(array)
    }
    
    for i in 0...cases - 1 {
        let rows    = grid[i][0]
        let columns = grid[i][1]
        
        if rows == columns && rows > 1 {
            if rows % 2 == 0 {
                print("L")
            }
            else {
                print("R")
            }
        }

        else if rows > columns && columns > 1 {
            if columns % 2 == 0 {
                print("U")
            }
            else {
                print("D")
            }
        }

        else if columns > rows {
            if rows % 2 == 0 {
                print("L")
            }
            else {
                print("R")
            }
        }
        
        else if columns == 1 {
            if rows == 1 {
                print("R")
            }
            else {
                print("D")
            }
        }
    }
}

transversedLines(cases: 4)

